#include <iostream>
#include <queue>
using namespace std;

struct BTNode
{
    BTNode(int num) : left(0), right(0), data(num) {}
    BTNode* left;
    BTNode* right;
    int data;
};

bool firstTreeContainsSecondTreeUtil(BTNode* tree, BTNode* treeToMatch)
{
    if(!treeToMatch)
        return true;

    if(!tree)
        return false;

    return ( tree->data == treeToMatch->data &&
            firstTreeContainsSecondTreeUtil(tree->left, treeToMatch->left) &&
            firstTreeContainsSecondTreeUtil(tree->right, treeToMatch->right) );
}
bool firstTreeContainsSecondTree(BTNode* tree, BTNode* treeToMatch)
{
    if(tree && treeToMatch)
    {
        queue<BTNode*> myQ;
        myQ.push(tree);
        while(!myQ.empty())
        {
            BTNode* root = myQ.front();
            myQ.pop();
            if(root)
            {
                if(firstTreeContainsSecondTreeUtil(root, treeToMatch))
                    return true;

                if(root->left)
                    myQ.push(root->left);

                if(root->right)
                    myQ.push(root->right);
            }
        }
    }
    else
    {
        if(!tree && !treeToMatch)
            cout << "Both Tree Are NULL" << endl;
        else
            cout <<"One of the tree is null" << endl;

        return false;
    }

    return false;
}

int main()
{
    //Source Tree
    /*                     1
     *                    / \
     *                  5     8
     *                 /     / \
     *                4     3   6 */

    BTNode sourceNode1(1);
    BTNode sourceNode2(5);
    BTNode sourceNode3(8);
    BTNode sourceNode4(4);
    BTNode sourceNode5(3);
    BTNode sourceNode6(6);

    sourceNode1.left = &sourceNode2;
    sourceNode1.right = &sourceNode3;
    sourceNode2.left = &sourceNode4;
    sourceNode3.left = &sourceNode5;
    sourceNode3.right = &sourceNode6;

    BTNode tree1ToMatch1(5);
    BTNode tree1ToMatch2(4);
    BTNode tree1ToMatch3(6);
    tree1ToMatch1.left = &tree1ToMatch2;

    /*
     *      5
     *     /
     *    4
     *                       */

    cout << "first Match both valid tree" << endl;
    cout << firstTreeContainsSecondTree(&sourceNode1, &tree1ToMatch1) << endl;

    /*
     *      5
     *     /
     *    7
     *                       */
    cout << "Second Match both valid tree" << endl;
    tree1ToMatch2.data = 7;
    cout << firstTreeContainsSecondTree(&sourceNode1, &tree1ToMatch1) << endl;

    /*
     *      8
     *     / \
     *    3   6
     *                       */
    tree1ToMatch1.data = 8;
    tree1ToMatch2.data = 3;
    tree1ToMatch1.right = &tree1ToMatch3;

    cout << "Third Match both valid tree" << endl;
    cout << firstTreeContainsSecondTree(&sourceNode1, &tree1ToMatch1) << endl;

    cout << "4th Match tree to match is NULL" << endl;
    cout << firstTreeContainsSecondTree(&sourceNode1, 0) << endl;

    cout << "5th Match source tree is NULL" << endl;
    cout << firstTreeContainsSecondTree(0, &tree1ToMatch1) << endl;

    cout << "6th Match both tree are NULL" << endl;
    cout << firstTreeContainsSecondTree(0, 0) << endl;

    return 0;
}

