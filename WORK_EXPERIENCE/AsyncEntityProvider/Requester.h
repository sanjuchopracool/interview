#ifndef REQUESTER_H
#define REQUESTER_H

#include <QWidget>

#include "EntityProvider.h"

namespace Ui {
class Requester;
}

class Requester : public QWidget
{
    Q_OBJECT

public:
    explicit Requester(QWidget *parent = 0);
    ~Requester();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void onDataReturned(quint64 id, QVariant inData);
private:
    Ui::Requester *ui;
    EntityProvider* provider;
    quint64 currentId;
};

#endif // REQUESTER_H
