#include "EntityProvider.h"
#include <QMetaMethod>

EntityProvider::EntityProvider(QObject *inParent)
    : QObject(inParent)
{

}

QVariant EntityProvider::getData(int)
{
    return QVariant(123);
}

quint64 EntityProvider::getAsyncData(int)
{
    requestCounter++;

    QMetaObject::invokeMethod(this,
                              "dataProcessed",
                              Qt::QueuedConnection,
                              Q_ARG(quint64, requestCounter),
                              Q_ARG(QVariant, QVariant(213123))
                              );
    return requestCounter;
}
