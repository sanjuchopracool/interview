#include "Requester.h"
#include "ui_Requester.h"

Requester::Requester(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Requester)
{
    ui->setupUi(this);
    provider = new EntityProvider(this);
}

Requester::~Requester()
{
    delete ui;
}

#include <QDebug>
void Requester::on_pushButton_clicked()
{
    currentId = provider->getAsyncData(4234);
    connect(provider, SIGNAL(dataProcessed(quint64,QVariant)), this, SLOT(onDataReturned(quint64,QVariant)));
    qDebug() << "I am done in this call";
}

void Requester::on_pushButton_2_clicked()
{
    ui->label->setText(provider->getData(12).toString());
}

void Requester::onDataReturned(quint64 id, QVariant inData)
{
    if( id == currentId)
    {
        disconnect(provider, SIGNAL(dataProcessed(quint64,QVariant)), this, SLOT(onDataReturned(quint64,QVariant)));
        ui->label->setText(inData.toString());
    }
}
