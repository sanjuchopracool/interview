#include <QApplication>
#include "Requester.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Requester requester;
    requester.show();
    return a.exec();
}
