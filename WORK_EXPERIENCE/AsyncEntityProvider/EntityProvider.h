#ifndef ENTITYPROVIDER_H
#define ENTITYPROVIDER_H

#include <QObject>
#include <QVariant>

class EntityProvider :  public QObject
{
    Q_OBJECT

public:
    EntityProvider( QObject* inParent = 0);

    QVariant getData(int id);

    quint64 getAsyncData(int);

signals:
    void dataProcessed(quint64, QVariant);

private:
    quint64 requestCounter = 0;
};

#endif // ENTITYPROVIDER_H
