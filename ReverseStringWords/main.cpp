#include <iostream>

using namespace std;

void reverseString(char* str, int length)
{
    int i = 0;
    int j = length -1;
    for(; i < j; i++, j--)
    {
        char ch = str[i];
        str[i] = str[j];
        str[j] = ch;
    }
}

void reverseStringWords(char* str, int length)
{
    reverseString(str, length);
    int previousSpaceIndex = -1;
    int currentSpaceIndex = 0;
    for(int i = 0; i < length -1; i++)
    {
        char ch = str[i];
        if(ch == ' ') // if its a space
        {
            currentSpaceIndex = i;
            reverseString(&str[previousSpaceIndex + 1], currentSpaceIndex - previousSpaceIndex - 1);
            previousSpaceIndex = currentSpaceIndex;
        }
    }
}

int main()
{
    string str = "I am Sanjay Chopra";
    reverseStringWords(&str[0], str.length());
    cout << str << endl;
    return 0;
}

