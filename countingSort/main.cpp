#include <iostream>

using namespace std;
void printArray(int array[], int size)
{
    for(int i = 0; i < size; i++)
        cout << array[i] << " ";
    cout << endl;
}

void countingSort(int A[], int size, int B[], int maxValue)
{
    int C[maxValue] = {};
    for(int i = 0; i < size; i++)
        C[A[i]] = C[A[i]] + 1;

    printArray(C, maxValue);
    // c containes the index  is containd the index value times in input array
    for(int i = 1; i < maxValue; i++)
        C[i] = C[i] + C[i -1];
    // c contains the no of items less then the index in input array
    printArray(C, maxValue);
    for(int j = size -1; j >= 0; j--)
    {
        B[C[A[j]]] = A[j];
        C[A[j]] = C[A[j]] - 1;
    }
    printArray(B, size);

}

int main()
{
    int A[10] = {1, 3, 4, 7, 3, 12 , 2, 17, 19, 11};
    int B[10] = {};
    cout << "A[] = ";
    printArray(A, 10);
    cout << "B[] = ";
    printArray(B, 10);
    countingSort(A, 10, B, 20);
    return 0;
}

