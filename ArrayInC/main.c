#include <stdio.h>

int main(void)
{
    int values[] = {12, 23 ,434 ,5 , 45 ,65 , 23, 66, 75, 23};
//    int values[10] = {12, 23 ,434 ,5 , 45 ,65 , 23, 66, 75, 23}; // can also be done like this

    int i = 0;
    int j = 0;
    for( i = 0; i < 10; i++)
        printf("values[%d] = %d\n", i, values[i]);

    printf("We can not increment array name like values++.\n"
           "but we can increment a pointer pa = values like pa++\n");


    printf("int* pa = (values + 1)  and *pa = %d\n", *(values + 1));

    // ANOTHER WAY OF TRAVERSING IS
    for( i = 0; i < 10; i++)
        printf("*(values + %d) = %d\n", i, *(values + i));

    // MULTIDIMENSIONAL ARRAY /////
//    int array1[2][3] = { 1, 2, 3 ,4 ,5 ,6 }; //this method also works fine
    int array1[2][3] = { {1, 2, 3} , {4 ,5 ,6} }; //but this is a better method
    for(i = 0; i < 2; i++)
    {
        for(j =0 ; j < 3; j++)
            printf("array1[%d][%d] = %d\n", i, j, array1[i][j]);
    }

    int* pa = array1[0];
    printf("int* pa = array1\n");
    printf("*(pa + 1) = %d\n", *(pa + 1));
    printf("*(pa + 2) = %d\n", *(pa + 2));
    printf("*(pa + 3) = %d. This is the first element of second row(Its Linear Memory)\n", *(pa + 3));
    printf("array1[0] is a pointer to first element of first row and its value is = %d\n", *array1[0]);
    printf("array1[1] is a pointer to first element of second row and its value is = %d\n", *array1[1]);
    printf("second element of first row array1[0][1] = %d\n", array1[0][1]);

    // array strings
    char* strings[] = { "Sanjay", "Satish", "Sunil", "Sanjeev"};
    for(i = 0; i < 4; i++)
        printf("strings[%d] = %s\n", i, strings[i]);

    printf("strings array have different length for each string\n");
    printf("*strings++ = %s\n", (*strings)++);
    printf("*strings = %s\n", *strings);
    printf("++(*strings) = %s\n", ++(*strings));

    // reset the pointer
    (*strings)--;
    (*strings)--;
//    printf("*(strings++) = %s\n", *(strings++)); // we can not increment array
    printf("*(strings + 1) = %s\n", *(strings + 1));
    char** pStr = strings;
    printf("pointer pStr is reset again, *pStr = %s\n", *pStr);
    printf("*(++pStr) = %s\n", *(++pStr));
    printf("++*pStr = %s (Right to left oeprator)\n", ++*pStr);
    printf("++*pStr = %s\n", *(++pStr));
    printf("++*pStr = %s\n", *(++pStr));
    return 0;
}

