#include <iostream>

using namespace std;

struct Length {
    Length() : left(0), right(0), index(-1)
    {}
    int left;
    int right;
    int index;

    int length() const {
        int len = left + right;
        if(len)
            len += 1;

        return len;
    }

    void reset() {
        left = 0;
        right = 0;
        index = -1;
    }

    void print() {
        cout << "Length After replacing = " << length() << ", index to replace = " << index << endl;
    }
};

int main()
{
    string buffer;
    cin >> buffer;
    cout << buffer <<endl;

    Length result;
    int bufferLength = buffer.length();
    int start = -1;
    for(int i =0; i < bufferLength; i++)
    {
        if(buffer[i] == '1')
        {
            start = i;
            break;
        }
    }

    if(start == -1)
    {
        cout << "Does not contains 1";
        return 0;
    }

    Length temp;
    for(int i = 0; i < bufferLength; i++)
    {
        char ch = buffer[i];
        if(ch == '1')
        {
            if(temp.index == -1)
                temp.left++;
            else
                temp.right++;
        }
        else
        {
            if(temp.length())
            {
                if(temp.index == -1)
                    temp.index = i;
                else
                {
                    if(temp.length() > result.length())
                    {
                        result = temp;
                        temp.reset();
                        temp.left = result.right;
                        temp.index = i;
                    }
                    else
                        temp.reset();
                }
            }
        }
    }

    if(temp.length() > result.length())
        result = temp;

    if(result.index == -1)
    {
        // 11111 pattern is the last
        if(bufferLength == result.left)
            cout << "Complete string contains 1" << endl;
        else
            result.index = bufferLength - result.left - 1;
    }

    result.print();
    return 0;
}

