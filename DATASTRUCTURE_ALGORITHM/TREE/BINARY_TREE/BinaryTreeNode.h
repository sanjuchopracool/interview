#ifndef BTNode_H
#define BTNode_H

#include <vector>
#include <string>

template<typename T>
struct BTNode
{
    BTNode(const T &inData): data(inData)
    {

    }

    T data;
    BTNode* left = nullptr;
    BTNode* right = nullptr;
};


BTNode<int>* createBalancedTree()
{
    using namespace std;
    string tree =
            R"(
                  BALANCED BINARY TREE

                        1
                       /  \
                      /    \
                     /      \
                    /        \
                   /          \
                  /            \
                 /              \
                /                \
               2                  3
              / \                / \
             /   \              /   \
            4     5            6     7
                 / \          / \
                /   \        /   \
               8     9      10   11
            )";
    cout << tree << endl;
    using NodePtr = BTNode<int>*;
    std::vector<NodePtr> nodesList;
    nodesList.resize(11);
    //check book page:119 (Data Structure made easy C/C++)

    for(int i = 0; i < 11; ++i)
    {
        nodesList[i] = new BTNode<int>(i+1);
    }
    // make the structure given in the book
    NodePtr root = nodesList.at(0);
    root->left = nodesList.at(1);
    root->right = nodesList.at(2);

    NodePtr node = nodesList.at(1);
    node->left = nodesList.at(3);
    node->right = nodesList.at(4);

    node = nodesList.at(2);
    node->left = nodesList.at(5);
    node->right = nodesList.at(6);

    node = nodesList.at(4);
    node->left = nodesList.at(7);
    node->right = nodesList.at(8);

    node = nodesList.at(5);
    node->left = nodesList.at(9);
    node->right = nodesList.at(10);

    return root;
}


BTNode<int>* createUnbalancedTree()
{
    using namespace std;
    string tree =
            R"(
                  UNBALANCED BINARY TREE

                        1
                       /  \
                      /    \
                     /      \
                    /        \
                   /          \
                  /            \
                 /              \
                /                \
               2                  3
              / \                / \
             /   \              /   \
            4     5            6     7
                 / \          /
                /   \        /
               8     9      10
                              \
                               \
                               11
            )";
    cout << tree << endl;

    using NodePtr = BTNode<int>*;
    std::vector<NodePtr> nodesList;
    nodesList.resize(11);
    //check book page:119 (Data Structure made easy C/C++)

    for(int i = 0; i < 11; ++i)
    {
        nodesList[i] = new BTNode<int>(i+1);
    }
    // make the structure given in the book
    NodePtr root = nodesList.at(0);
    root->left = nodesList.at(1);
    root->right = nodesList.at(2);

    NodePtr node = nodesList.at(1);
    node->left = nodesList.at(3);
    node->right = nodesList.at(4);

    node = nodesList.at(2);
    node->left = nodesList.at(5);
    node->right = nodesList.at(6);

    node = nodesList.at(4);
    node->left = nodesList.at(7);
    node->right = nodesList.at(8);

    node = nodesList.at(5);
    node->left = nodesList.at(9);

    node = nodesList.at(9);
    node->right = nodesList.at(10);

    return root;
}

BTNode<int>* createSymmetricTree()
{
    using namespace std;
    string tree =
            R"(
                  SYMMETRIC BINARY TREE

                        1
                       /  \
                      /    \
                     /      \
                    /        \
                   /          \
                  /            \
                 /              \
                /                \
               5                  5
              / \                / \
             /   \              /   \
            4     7            7     4
                 / \          / \
                /   \        /   \
               8     9      9     8
            )";

    cout << tree << endl;

    using NodePtr = BTNode<int>*;
    std::vector<NodePtr> nodesList;
    nodesList.resize(11);
    //check book page:119 (Data Structure made easy C/C++)

    for(int i = 0; i < 11; ++i)
    {
        nodesList[i] = new BTNode<int>(i+1);
    }
    // make the structure given in the book
    NodePtr root = nodesList.at(0);

    nodesList[1]->data = 5;
    nodesList[2]->data = 5;
    root->left = nodesList.at(1);
    root->right = nodesList.at(2);

    nodesList[3]->data = 4;
    nodesList[4]->data = 7;
    NodePtr node = nodesList.at(1);
    node->left = nodesList.at(3);
    node->right = nodesList.at(4);

    nodesList[5]->data = 7;
    nodesList[6]->data = 4;
    node = nodesList.at(2);
    node->left = nodesList.at(5);
    node->right = nodesList.at(6);

    nodesList[7]->data = 8;
    nodesList[8]->data = 9;
    node = nodesList.at(4);
    node->left = nodesList.at(7);
    node->right = nodesList.at(8);

    nodesList[9]->data = 9;
    nodesList[10]->data = 8;
    node = nodesList.at(5);
    node->left = nodesList.at(9);
    node->right = nodesList.at(10);

    return root;
}

#endif // BTNode_H
