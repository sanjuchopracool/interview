#include <iostream>
#include "../BinaryTreeNode.h"
#include <queue>

using namespace std;

/////////////// LEVEL ORDER TRAVERSAL BINARY TREE ////////////////////
template <typename T>
void LevelOrderTraversal(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        queue<BTNode<T>*> nodes;
        nodes.push(inRoot);
        while( !nodes.empty())
        {
            // NOTE pop works for queue also
            BTNode<T>* node = nodes.front();
            nodes.pop();

            cout << node->data << " ";

            if(node->left)
                nodes.push(node->left);

            if(node->right)
                nodes.push(node->right);
        }
    }
}

/////////////// LEVEL ORDER RIGHT TO LEFT TRAVERSAL BINARY TREE ////////////////////
template <typename T>
void LevelOrderReverseTraversal(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        queue<BTNode<T>*> nodes;
        nodes.push(inRoot);
        while( !nodes.empty())
        {
            // NOTE pop works for queue also
            BTNode<T>* node = nodes.front();
            nodes.pop();

            cout << node->data << " ";

            if(node->right)
                nodes.push(node->right);

            if(node->left)
                nodes.push(node->left);
        }
    }
}

///////////////////////// PRINT RIGHT NODE /////////////////////////////////////////
template <typename T>
void rightView(BTNode<T>* inRoot)
{
    while(inRoot)
    {
        cout << inRoot->data <<" ";
        inRoot = inRoot->right;
    }
}

using BTPtr = BTNode<int>*;

/// INORDER TRAVERSAL
#include <stack>

#include <string>
void printStack( stack<BTPtr> inNodes )
{
    BTPtr node;
    string data;
    while(inNodes.size())
    {
        node = inNodes.top();
        if (node)
            data  = to_string(node->data) + " " + data;

        inNodes.pop();
    }

    cout << endl << "Stack DATA " << data << endl;
}

void preOrderTraversal(BTPtr inRoot)
{
    cout << endl << "PreOrderTraversal" << endl;
    stack<BTPtr> nodes;
    nodes.push(inRoot);
    BTPtr temp;
    while(!nodes.empty())
    {
//        printStack(nodes);
        temp = nodes.top();
        nodes.pop();

        if( temp )
        {
            cout << temp->data << " ";

            // We need to first push right and then left
            // because we want to process left first and
            // pop will return the left first;

            nodes.push(temp->right);
            nodes.push(temp->left);
        }
    }

}

void inOrderTraversal(BTPtr inRoot)
{
    cout << endl << "InOrderTraversal" << endl;
    stack<BTPtr> nodes;
    while(1)
    {
        while(inRoot)
        {
            nodes.push(inRoot);
            inRoot = inRoot->left;
        }

        if( nodes.empty() )
            break;

        inRoot = nodes.top();
        nodes.pop();

        cout << inRoot->data << " ";
        inRoot = inRoot->right;
    }

    cout << endl;
}

void postOrderTraversal(BTPtr root)
{
    cout << endl << "postOrderTraversal" << endl;
    stack<BTPtr> nodes;
    while(1)
    {
        if(root)
        {
            nodes.push(root);
            root = root->left;
        }
        else
        {
            if(nodes.empty())
                return;

            // Right child is not there
            // So process it
            root = nodes.top();
            if(!root->right)
            {
                nodes.pop();
                cout << root->data << " ";

                // if the right of top node
                // is not the processed node
                while(!nodes.empty() && (root == nodes.top()->right))
                {
                    root = nodes.top();
                    nodes.pop();
                    cout << root->data << " ";
                }
            }

            // if right is there
            // this part will handle the
            // traversal through the right node
            if(!nodes.empty())
            {
                root = nodes.top()->right;
            }
            else
                root = 0;
        }
    }
}

int main()
{
    auto root = createBalancedTree();

    preOrderTraversal(root);
    cout << endl;

    inOrderTraversal(root);
    cout << endl;

    postOrderTraversal(root);
    cout << endl;

    cout << endl << "Level order traversal" << endl;
    LevelOrderTraversal(root);
    cout << endl << endl;

    cout << "Level order reverse traversal" << endl;
    LevelOrderReverseTraversal(root);
    cout << endl << endl;

    cout << "Right view" << endl;
    rightView(root);
    cout << endl << endl;

    return 0;
}
