TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

HEADERS += \
    ../BinaryTreeNode.h \
    heightbalanced.h \
    symmetricandmirror.h \
    haspathsum.h
