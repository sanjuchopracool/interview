#ifndef HEIGHTBALANCED_H
#define HEIGHTBALANCED_H

using namespace std;
#include "../BinaryTreeNode.h"

struct BalancedStatusWithHeight
{
    BalancedStatusWithHeight(bool inBalanced, int inHeight) : balanced(inBalanced), height(inHeight)
    {

    }

    bool balanced = false;
    int height = -1;
};

BalancedStatusWithHeight CheckBalanced(BTNode<int>* inRoot)
{
    if ( !inRoot )
        return {true, -1};

    auto left_result = CheckBalanced(inRoot->left);
    if(!left_result.balanced )
        return {false, 0};

    auto right_result = CheckBalanced(inRoot->right);
    if(!right_result.balanced )
        return {false, 0};

    bool isBalanced = std::abs(left_result.height - right_result.height) <= 1;
    int height = max(left_result.height, right_result.height) + 1;
    return {isBalanced,height};
}

bool isHeightBalanced(BTNode<int>* inRoot)
{
    auto result = CheckBalanced(inRoot);
    if( result.balanced )
    {
        cout << endl << "Tree is balanced with Height: " << result.height << endl;
        return true;
    }
    else
    {
        cout << endl << "Tree is not balanced with Height: " << result.height << endl;
        return false;
    }
}

#endif // HEIGHTBALANCED_H
