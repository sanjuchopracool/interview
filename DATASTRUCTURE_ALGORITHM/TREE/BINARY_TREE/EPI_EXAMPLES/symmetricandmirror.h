#ifndef SYMMETRICANDMIRROR_H
#define SYMMETRICANDMIRROR_H

#include "../BinaryTreeNode.h"

using BTPtr = BTNode<int>*;

bool areMirror( BTPtr tree1, BTPtr tree2)
{
    if ( !tree1 && !tree2 )
        return true;

    if( !tree1 || !tree2 )
        return false;

    return ( (tree1->data == tree2->data) &&
             (areMirror(tree1->left, tree2->right)) &&
             (areMirror(tree1->right, tree2->left)));
}

//NOTE can level order can also be used where the container contianing
//  all the nodes in a level is palindrome

bool isSymmetric(BTPtr inTree)
{
    bool result = !inTree || areMirror(inTree->left, inTree->right);
    cout << endl << "   Tree is symmetric : " << result << endl;
    return result;
}


#endif // SYMMETRICANDMIRROR_H
