#include <iostream>
#include "../BinaryTreeNode.h"
#include "heightbalanced.h"
#include "symmetricandmirror.h"
#include "haspathsum.h"

using namespace std;

// check for height balance for a tree
void symmetricAndMirrorTester()
{
    BTNode<int>* balancedTree = createBalancedTree();
    isSymmetric(balancedTree);

    BTNode<int>* unbalancedTree = createUnbalancedTree();
    isSymmetric(unbalancedTree);

    BTNode<int>* symmetricTree = createSymmetricTree();
    isSymmetric(symmetricTree);

    cout << "   Checking mirror : " << areMirror(symmetricTree->left, symmetricTree->right) << endl;;
}

// check for symmetric and mirror for BT
void heightBalancedTester()
{
    BTNode<int>* balancedTree = createBalancedTree();
    isHeightBalanced(balancedTree);

    BTNode<int>* unbalancedTree = createUnbalancedTree();
    isHeightBalanced(unbalancedTree);


    //
    BTNode<int>* newNode, *root;
    newNode = root = new BTNode<int>(2);
    isHeightBalanced(root);

    newNode = newNode->left = new BTNode<int>(4);
    isHeightBalanced(root);

    newNode = newNode->left = new BTNode<int>(5);
    isHeightBalanced(root);
}

// check for path sum
void checkHasPathSum()
{
    BTNode<int>* unbalancedTree = createUnbalancedTree();
    cout << "   has sum 7 " << hasPathSum(unbalancedTree, 7) << endl;
    cout << "   has sum 10 " << hasPathSum(unbalancedTree, 10) << endl;
    cout << "   has sum 31 " << hasPathSum(unbalancedTree, 31) << endl;
    cout << "   has sum 13 " << hasPathSum(unbalancedTree, 13) << endl;
}

int main()
{
    cout << boolalpha;
    heightBalancedTester();
    symmetricAndMirrorTester();
    checkHasPathSum();
    return 0;
}
