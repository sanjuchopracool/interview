#ifndef HASPATHSUM_H
#define HASPATHSUM_H


#include "../BinaryTreeNode.h"

using BTPtr = BTNode<int>*;

// Find if there exist a sum from root to leaf node

bool hasPathSum( BTPtr root, int sum )
{
    if ( !root )
        return false;

    if ( !root->left && !root->right)
        return root->data == sum;

    int sumLeft = sum - root->data;
    return hasPathSum( root->left, sumLeft ) ||
           hasPathSum( root->right, sumLeft );
}

#endif // HASPATHSUM_H
