#include <iostream>
#include "../BinaryTreeNode.h"

using namespace std;

///////////// PERORDER TRAVERSAL ////////////////////////////
template<typename T>
void preOrderTraversal(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        cout << inRoot->data << " ";
        preOrderTraversal(inRoot->left);
        preOrderTraversal(inRoot->right);
    }
}

///////////// POSTORDER TRAVERSAL ////////////////////////////
template<typename T>
void postOrderTraversal(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        postOrderTraversal(inRoot->left);
        postOrderTraversal(inRoot->right);
        cout << inRoot->data << " ";
    }
}

///////////// INORDER TRAVERSAL ////////////////////////////
template<typename T>
void inOrderTraversal(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        inOrderTraversal(inRoot->left);
        cout << inRoot->data << " ";
        inOrderTraversal(inRoot->right);
    }
}

///////////// DELETE BINARY TREE ///////////////////////////
template<typename T>
void deleteBinaryTree(BTNode<T>* inRoot)
{
    // POST ORDER DELETE
    if( inRoot )
    {
        deleteBinaryTree(inRoot->left);
        deleteBinaryTree(inRoot->right);
        cout << "SHOULD DELETE NODE WITH DATA: "  << inRoot->data <<  endl;
    }
}

///////////// FINDING NODE IN BINARY TREE ///////////////////////////
template<typename T>
BTNode<T>* findinBinaryTree(BTNode<T>* inRoot, const T& data)
{
    // POST ORDER DELETE
    if( inRoot )
    {
        cout << "COMPARING WITH:" << inRoot->data << endl;
        if ( inRoot->data == data)
            return inRoot;

        auto temp = findinBinaryTree( inRoot->left, data );
        if(temp)
            return temp;

        return findinBinaryTree(inRoot->right, data);;
    }

    return nullptr;
}

///////////// FINDING SIZE OF BINARY TREE ///////////////////////////
template<typename T>
std::size_t sizeofBinaryTree(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        return ( sizeofBinaryTree(inRoot->left) + 1 + sizeofBinaryTree(inRoot->right));
    }

    return 0;
}

///////////// FINDING SIZE OF BINARY TREE ///////////////////////////
template<typename T>
std::size_t heightofBinaryTree(BTNode<T>* inRoot)
{
    if( inRoot )
    {
        auto leftHeight = heightofBinaryTree(inRoot->left);
        auto rightHeight = heightofBinaryTree(inRoot->right);
        if( leftHeight >= rightHeight )
            return leftHeight + 1;

        return rightHeight + 1;
    }

    return 0;
}

//////////////////////// PRINT Ancestors ///////////////////////////////
template<typename T>
bool printAncestors(BTNode<T>* inRoot, const T & inData)
{
    if(inRoot)
    {
        if( inRoot->data == inData )
            return true;

        if( printAncestors(inRoot->left, inData) ||
            printAncestors(inRoot->right, inData))
        {
            cout << inRoot->data  << " ";
            return true;
        }
    }

    return false;
}

///////////////////////// FIND PATH from root to node /////////////////////////
template<typename T>
bool findPath(BTNode<T>* inRoot, vector<BTNode<T>*>& inPath, const T & target)
{
    if( inRoot )
    {
        inPath.push_back(inRoot);
        if ( inRoot->data == target )
            return true;

        if((inRoot->left && findPath(inRoot->left, inPath, target)) ||
           (inRoot->right && findPath(inRoot->right, inPath, target)) )
            return true;

        // we could not fine path so pop
        inPath.pop_back();
    }
    return false;
}

//////////////////////// Least common Ancestors ///////////////////////////////
template<typename T>
void printLeastCommonAncestor(BTNode<T>* inRoot, const T & target1, const T & target2)
{
    if (inRoot)
    {
        vector<BTNode<int>*> target1Path;
        vector<BTNode<int>*> target2Path;

        if(!findPath(inRoot, target1Path, target1) ||
           !findPath(inRoot, target2Path, target2) )
        {
            cout << "Could not find common path for " << target1 << " and " << target2 <<endl;
            return;
        }

        int i = 0;
        while( target1Path[i] == target2Path[i])
            ++i;

        cout << target1Path[i-1]->data;
    }
}

int main()
{
    auto root = createBalancedTree();
    preOrderTraversal<int>(root);
    cout << endl;

    postOrderTraversal<int>(root);
    cout << endl;

    inOrderTraversal<int>(root);
    cout << endl;

    deleteBinaryTree<int>(root);
    cout << endl;

    cout << "FINDING 7 : " << endl;
    if( auto found = findinBinaryTree<int>(root, 7))
        cout << endl << "FOUND : " << found << " WITH DATA " << found->data << endl;

    cout << endl << "Size of binary tree : " <<  sizeofBinaryTree( root ) << endl;
    cout << endl << "Height of binary tree : " <<  heightofBinaryTree( root ) << endl;

    cout << endl << "Ancestors of 9: " << endl;
    printAncestors(root, 9);
    cout << endl;

    cout << endl << "Finding Path for 9: " << endl;
    vector<BTNode<int>*> path;
    if( findPath<int>(root, path,9))
    {
        cout << "Found the following path for 9" << endl;
        for( const auto& node: path )
            cout << node->data << " ";
        cout << endl;
    }
    cout << endl;

    cout << "Least common ancestor for 9, 10 is ";
    printLeastCommonAncestor<int>(root, 9, 10);
    cout << endl << endl;

    cout << "Least common ancestor for 9, 8 is ";
    printLeastCommonAncestor<int>(root, 9, 8);
    cout << endl << endl;

    cout << "Least common ancestor for 6, 7 is ";
    printLeastCommonAncestor<int>(root, 6, 7);
    cout << endl << endl;

    return 0;
}
