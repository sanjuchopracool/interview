#include <iostream>

using namespace std;

unsigned BruteForceDivide( unsigned a, unsigned b)
{
    unsigned result = 0;
    while( a >= b )
    {
        a -= b;
        result++;
    }

    return result;

    // Time complexity  O(a/b)
}

int main()
{
    cout << "divide( 235, 2) = " << BruteForceDivide(235, 2) << endl;
    return 0;
}
