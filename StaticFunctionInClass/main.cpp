#include <iostream>
#include <MyClass.h>

using namespace std;

int main()
{
    MyClass::printAndIncreaseStaticMember(); //its use static data member and works find
    MyClass::printLocalDataMemberUsingStatic(); //Compilation error (commented in implementation CHECK CLASS IMPLEMENTATION)
    MyClass myObject(234);
    myObject.printAndIncreaseDataMemeber();
    return 0;
}

