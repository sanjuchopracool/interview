#ifndef MYCLASS_H
#define MYCLASS_H

class MyClass
{
public:
    MyClass(int val = 0);
    ~MyClass();

    static void printAndIncreaseStaticMember();
    static void printLocalDataMemberUsingStatic();
    void printAndIncreaseDataMemeber();

private:
    static int varStatic_;
    int varData;
};

#endif // MYCLASS_H
