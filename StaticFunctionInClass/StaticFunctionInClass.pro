TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    MyClass.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    MyClass.h

OTHER_FILES += \
    Observation.txt

