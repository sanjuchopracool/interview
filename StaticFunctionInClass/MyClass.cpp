#include "MyClass.h"
#include <iostream>

using namespace std;
int MyClass::varStatic_ = 23;

MyClass::MyClass(int val) :
    varData(val)
{
}

MyClass::~MyClass()
{

}

void MyClass::printAndIncreaseStaticMember()
{
    std::cout << varStatic_++ << endl;
}

void MyClass::printLocalDataMemberUsingStatic()
{
//    cout << varData << endl; //it gives a compilation error
//    printAndIncreaseDataMemeber(); //function also gives compilation error
}

void MyClass::printAndIncreaseDataMemeber()
{
    cout << varData++ << endl;
}
